<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Categories;
use App\Images;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Categories::all();
        return view('home', compact('categories'));
    }

    public function addCategory(Request $request)
    {
        $data = $request->all();
        $category = new Categories;
        $category->name = $data['name'] ?? '';
        $category->save();
        $categories = Categories::all();
        return response()->json(['success' => true, 'message' => $categories]);
    }

    public function deleteCategory(Request $request)
    {
        $data = $request->all();
        $category = Categories::find($data['id']);
        $category->delete();
        $categories = Categories::all();
        return response()->json(['success' => true, 'message' => $categories]);
    }

    public function addImage($category_id, Request $request)
    {
        $file = $request->file('file');
        $filename = time().'.'.$file->getClientOriginalExtension();
        $path = $file->storeAs('public', $filename);
        $realPath = Storage::url($path);
        $image = new Images;
        $image->category_id = $category_id;
        $image->file = $realPath;
        $image->save();
        $images = Images::where('category_id', $category_id)->get()->toArray();
        return ['success' => true, 'message' => $images];
    }

    public function fetchImages($category_id)
    {
        $images = Images::where('category_id', $category_id)->get()->toArray();
        return ['success' => true, 'message' => $images];
    }

    public function saveImageDetails(Request $request)
    {
        $image = Images::find($request->id);
        $image->details = $request->details;
        $image->save();
        return ['success' => true, 'message' => $image];
    }
}
