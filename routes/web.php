<?php
use App\Categories;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $categories = Categories::all();
    return view('home', compact('categories'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/add-category', 'HomeController@addCategory')->name('home.add-category');
Route::post('/delete-category', 'HomeController@deleteCategory')->name('home.delete-category');
Route::post('/add-image/{category_id}', 'HomeController@addImage')->name('home.add-image');
Route::post('/save-image-details', 'HomeController@saveImageDetails')->name('home.save-image-details');
Route::get('/fetch-images/{category_id}', 'HomeController@fetchImages')->name('home.fetch-images');
