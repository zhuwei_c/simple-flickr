@extends('layouts.app')

@section('content')
    <div id="vue-app">
        <flickr-component status="{{ Auth::check() }}" :list="{{ $categories }}"></flickr-component>
    </div>
@endsection
