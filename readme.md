## 1.
Clone this project into your localhost folder

## 2.
Open terminal, cd to the root folder of this project:
    1. run 'npm install' to install dependencies.
    2. run 'composer install' to install dependencies.
    3. run 'sudo chmod -R 777 storage' to give permissions to storage folder
    4. run 'php artisan storage:link' to link your storage to your public assets folder.

## 3.
Rename '.env.example' to '.env'.
Open file '.env' to setup your database login.
Config the below part in '.env' file:

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=flickr
DB_USERNAME=root
DB_PASSWORD=

## 4.
Still under the root folder of this project, run 'php artisan migrate' to migrate the data tables into your database.

## 5.
Still under the root folder of this project, run 'php artisan serve' to start the server and then go to a browser and access the url "http://127.0.0.1:8000/".

## 6.
Click 'Register' on the top right corner of the page and register an account for you to manage the flickr.

## 7.
You can start to add categories and images to the category.
